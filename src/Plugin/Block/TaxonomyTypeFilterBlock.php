<?php

namespace Drupal\type_filter\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Renderer;

/**
 * Provides a 'TaxonomyTypeFilterBlock' block.
 *
 * @Block(
 *  id = "taxonomy_type_filter_block",
 *  admin_label = @Translation("Taxonomy type filter block"),
 * )
 */
class TaxonomyTypeFilterBlock extends BlockBase implements ContainerFactoryPluginInterface {


  /**
   * Drupal\Core\Render\Renderer definition.
   *
   * @var Drupal\Core\Render\Renderer
   */
  protected $renderer;
  protected $request;
  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        Renderer $renderer
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $view = views_embed_view('taxonomy_term', 'embed_1', $path_args[3]);
    $build = [];
    $build['taxonomy_type_filter_block']['#markup'] =  ($view)?$this->renderer->render($view) : t("No content");

    return $build;
  }

}
