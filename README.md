IMPORTANT:


Will create a block with the views of taxonomy_term embed_1, so add it before
if not it will probably crash . As a TODO, let user choose the view to embed
as configuration.


As example , one can add this configuration:

```
  embed_1:
    display_plugin: embed
    id: embed_1
    display_title: Incrustrat
    position: 3
    display_options:
      display_extenders: {  }
      arguments:
        tid:
          id: tid
          table: taxonomy_index
          field: tid
          relationship: none
          group_type: group
          admin_label: ''
          default_action: 'not found'
          exception:
            value: ''
            title_enable: false
            title: Tot
          title_enable: true
          title: '{{ arguments.tid }}'
          default_argument_type: fixed
          default_argument_options:
            argument: ''
          default_argument_skip_url: false
          summary_options:
            base_path: ''
            count: true
            items_per_page: 25
            override: false
          summary:
            sort_order: asc
            number_of_records: 0
            format: default_summary
          specify_validation: true
          validate:
            type: 'entity:taxonomy_term'
            fail: 'not found'
          validate_options:
            access: true
            operation: view
            multiple: 0
            bundles: {  }
          break_phrase: false
          add_table: false
          require_value: false
          reduce_duplicates: false
          plugin_id: taxonomy_index_tid
        type:
          id: type
          table: node_field_data
          field: type
          relationship: none
          group_type: group
          admin_label: ''
          default_action: summary
          exception:
            value: all
            title_enable: false
            title: All
          title_enable: false
          title: ''
          default_argument_type: fixed
          default_argument_options:
            argument: ''
          default_argument_skip_url: false
          summary_options:
            base_path: ''
            count: true
            items_per_page: 25
            override: false
          summary:
            sort_order: asc
            number_of_records: 0
            format: default_summary
          specify_validation: false
          validate:
            type: none
            fail: 'not found'
          validate_options: {  }
          glossary: false
          limit: 0
          case: none
          path_case: none
          transform_dash: false
          break_phrase: false
          entity_type: node
          entity_field: type
          plugin_id: node_type
      defaults:
        arguments: false
        header: false
      header: {  }
    cache_metadata:
      max-age: -1
      contexts:
        - 'languages:language_interface'
        - url
        - url.query_args
        - 'user.node_grants:view'
      tags: {  }
```
